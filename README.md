# sails-fileupload

a [Sails](http://sailsjs.org) application

############################################################################################################


Note: (1). On both server and front end, "file_param_name" used in the explanation below should be the same.

The Flow: 
(1). A server API should be set up to handle incoming file via HTTP "POST" request(Setting up the Server Side).
eg. function(req, res){ req.file('file').upload({}, function(e,f){//e=error, f=uploaded files, {} = options.
})};
(2). Http request from the client to the server(Setting up the client Side).
eg: <form method="POST" action="/file/upload" ><input name="file" type="file"><input type="submit" name="submit"></form>

Setting up the Server Side:
File upload is handled easily by sails through its built-in middleware called skipper(https://www.npmjs.com/package/skipper), which is a body parser. check the skipper options for advanced methods.



Steps to create the upload enpoint:
1). Generate/create a controller/api to handle the file upload ((or use any of your existing controller).
Example:
$ sails generate api file
This will generate FileController.js file in api/controllers folder and a model called File.js in api/models folder.

2). Create an upload api endpoint in the controller file.


Example:
In "api/controllers/FileController.js" file,

module.export = {
    upload: function(req, res) {
    	//input form data which contains file in the param name = "file_param_name", 
    	// i.e. req.param('file_param_name') should not be empty in this case.

        //file_param_name = the name of the input field from the html form from the front end.
        req.file('file_param_name').upload({
            // set the upload size limit. eg. don't allow the total upload size to exceed ~10MB
            maxBytes: 10000000, //10mb = 10000000, in bytes. optional.
            dirname: "path_to_store_file" 	// optional. you may use 'url' and 'path' npm to specify the location you want.
            								// if not specified, the uploaded file goes to ".tmp/uploads" folder.
        }, function(err, uploadedFiles) {
            //if all went well, "uploadedFiles" will contains an array of the file(s) uploaded.
            //	[{
            //	  fd: 'the_uploaded_file_path',
            //	  size: 17568504, //file size in bytes
            //	  type: 'image/jpeg', // file type
            //	  filename: 'f 4821 E.jpg', //filename of the incoming file
            //	  status: 'bufferingOrWriting', //status
            //	  field: 'file_param_name', //file param name from the request.
            //	  extra: undefined 
            // }] 
            if (err) {
                console.log(err);
                return res.send(err);
            }
            res.send(uploadedFiles);
        });
    }
}

3). In cases of mutiple files, if the size of the file to be uploaded is significant, network delay might cause the file listener on the api to timeout for the subsequent file parameters. It is the choice of the developer how to handle it, while keeping in mind the whole upload strategy revolves around the above point(i.e, pt.2). 
	(i). Sending an array of files from multiple file input can be done. Or 
	(ii). Sending an array of file from a single input file field with multiple file select is also an option.


#############################################################################################################################

Setting up the client Side:
You should have an HTML file and a JS file. The HTML file can be from a templating engine like ejs(in which case your html content should be located within views folder or view's subfolder(s) ).
Your JS files should be located within the assets folder. 

HTML: 
<!-- ------------------------------------------------------------------------------------------------------ -->

<!-- Single file upload -->
<form enctype="multipart/form-data">
	<input name="file_param_name" type="file" placeholder="chose file" >
	<input type="submit" name="submit" value="submit">
</form>

OR

<!-- Multiple file upload -->
<form enctype="multipart/form-data">
	<input name="file_param_name" type="file" placeholder="chose file" >
	<input name="file_param_name" type="file" placeholder="chose file"> 
	<!-- 
	please note the 'name' attribute being the same for both the input field for multiple file in this case 
	-->
	<input type="submit" name="submit" value="submit">
</form>

OR

<!-- Multiple file upload -->
<form enctype="multipart/form-data">
	<input name="file_param_name" type="file" placeholder="chose file" multiple>
	<!-- 
	please note the 'multiple' attribute being added in the input field for multiple file selection in this case 
	-->
	<input type="submit" name="submit" value="submit">
</form>
<!-- --------------------------------------------------------------------------------------------------------- -->

JS:
/* ************************************************************************************************************ */
// load Jquery first before your custom script file is loaded as the scripts written below is dependent on jquery.

// Next, see the code below. Comments/explations are provided as and where necessary.
$('input[type=submit]').on('click', function(e){ 	//form submit button selected and click event is bind to it.
 													//Your preferred selection method and event binding method can 
 													//be applied here.
	//prevent event default. in case if a form action and method are accidentally specified.
	e.preventDefault();			

	//Stop the click event to propagate to its parent element(s).
	e.stopPropagation();		
	
	//make use of the HTML5 form API to create a new form object. The HTML5 FormData takes DOM as input.
	//So, our selector has to return the html from the jquery object. Note the "[0]" in $('form')[0].
	var formData = new FormData($('form')[0]); 

	//Now, ajax call.
	$.ajax({

		//customizing the xhr in ajax.
  		xhr: function() {

  			//create new xhr object.
	    	var xhr = new window.XMLHttpRequest();

	    	//add progress listener to the newly created xhr object.
	    	xhr.upload.addEventListener("progress", function(evt) {
	      		
	      		if (evt.lengthComputable) {
	        		var percentComplete = evt.loaded / evt.total;
	        		
	        		//percentComplete will give you the percentage of data sent to the server.
	        		//This information can be used to tell the user of the progress.
	        		percentComplete = parseInt(percentComplete * 100);
	        		
	        		//data sending completed from the browser/http agent.
	        		if (percentComplete === 100) {
	        			//data successfully send. Do what you got to do.
	        		}

	      		}
	    	}, false);

	    	return xhr;
	  	},
  		url: 'url_of_server_upload_endpoint',	//eg. '/file/upload'
  		type: "POST",		//request method.
  		data: formData,		//post form data.
  		contentType: false, //auto detect it.
  		processData:false, //processData false is mandatory so that the data content will not be formatted by our ajax.
  		success: function(result) {
    		
    		//response received from the server on success.
    		console.log(result);
  		}
	});
});
/* ***************************************************************************************************************** */